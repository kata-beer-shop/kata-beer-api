## Kata Beer Api

## Author

- Victoria Doe

## Software

- Node v20.8.0 - Nest v8.1.5
- Typescript v4.3.5
- PostgresSQL
- Docker

## Subject

An en E-commerce API that allows customers to purchase beers and administrators to manage the store

## How to run the project

To run the project locally, first make sure you have Node and npm installed.  
Clone this [URL]("https://gitlab.com/kata-beer-shop/kata-beer-api.git")

You must first install all the node_modules by executing the following command :
```bash
  npm install
```

To launch the database and server, open a terminal at the root of the project (outside the src folder) :

```bash
  docker-compose build  
  docker-compose up
```

To run the project in development mode (watch mode i.e., each change to a file will restart the server and be instantly taken into account) :

```bash
  npm run start:dev
```

To run the project in production mode

```bash
  npm run start:prod
```

Retrieve container ids :

```bash
  docker ps
```

To shut the database and the server down :

```bash
  docker stop container-id
```

You will also need to set the following environment variables in a .env file at the root of the project (do not wrap variables in quotes):

| Environment Variable | Default Value | Type    |
| -------------------- | ------------- | ------- |
| PORT                 | 3005          | integer |
| NODE_ENV             | dev           | string  |
| POSTGRES_HOST        | hidden        | string  |
| POSTGRES_PORT        | 5432          | integer |
| POSTGRES_USERNAME    | hidden        | string  |
| POSTGRES_PASSWORD    | hidden        | string  |
| POSTGRES_DATABASE    | hidden        | string  |
| JWT_SECRET_KEY       | hidden        | string  |
| JWT_TOKEN_EXPIRATION | 24h           | string  |

This .env file is mandatory for the docker-compose.yml to start the database and the server

## Queries
1. Retrieve all beers  
2. Retrieve a beer by name  
3. Retrieve a user by email  
4. Create a user  
5. Log a user in  
6. Update its own beer

## Documentation
To access the swagger, simply open your browser and search for : localhost:3005/api (change port according to your env file)
