import { Action } from '../enum/action.enum'
import { AbilityBuilder, createMongoAbility, ExtractSubjectType, InferSubjects, MongoAbility } from '@casl/ability'
import { Beer } from '../../beer/entity/beer.entity'
import { User } from '../../user/entity/user.entity'
import { Injectable } from '@nestjs/common'

type Subjects = InferSubjects<typeof Beer | typeof User> | 'all'

export type AppAbility = MongoAbility<[Action, Subjects]>

@Injectable()
export class CaslAbilityFactory {
    createForUser(user: User) {
        const { can, build } = new AbilityBuilder<AppAbility>(createMongoAbility)

        can(Action.Read, Beer)
        can(Action.Read, User, { id: user.id })

        can(Action.Update, Beer, { owner_id: user.id })

        can(Action.Delete, Beer, { owner_id: user.id })

        return build({
            detectSubjectType: (object) => object.constructor as ExtractSubjectType<Subjects>
        })
    }
}
