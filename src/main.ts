import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ConfigService } from '@nestjs/config'
import { corsOptions } from './middleware/cors/cors'
import { ValidationPipe } from '@nestjs/common'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { writeFile } from 'fs/promises'
import { resolve } from 'path'

async function bootstrap() {
    const app = await NestFactory.create(AppModule)
    app.useGlobalPipes(new ValidationPipe({ whitelist: true }))

    const swaggerConfig = new DocumentBuilder()
        .setTitle('Kata Beer Api')
        .setDescription('An API that allows customers to purchase beers and administrators to manage the store')
        .setVersion('1.0.0')
        .addBearerAuth()
        .build()

    const swaggerDocument = SwaggerModule.createDocument(app, swaggerConfig)
    await writeFile(resolve(process.cwd(), 'swagger.json'), JSON.stringify(swaggerDocument))
    SwaggerModule.setup('api', app, swaggerDocument)

    app.enableCors(corsOptions())

    const configService: ConfigService = app.get(ConfigService)
    await app.listen(configService.get('PORT'))
}

bootstrap().then(console.log).catch(console.error)
