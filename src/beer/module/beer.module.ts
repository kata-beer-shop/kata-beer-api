import { Module } from '@nestjs/common'
import { BeerController } from '../controller/beer.controller'
import { BeerService } from '../service/beer.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Beer } from '../entity/beer.entity'
import { CaslModule } from '../../casl/module/casl.module'

@Module({
    imports: [TypeOrmModule.forFeature([Beer]), CaslModule],
    controllers: [BeerController],
    providers: [BeerService]
})
export class BeerModule {}
