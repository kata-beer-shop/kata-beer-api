import {
    Controller,
    Get,
    Param,
    NotFoundException,
    Post,
    Body,
    ConflictException,
    UnprocessableEntityException,
    Patch,
    Req,
    UnauthorizedException,
    UseInterceptors,
    ClassSerializerInterceptor
} from '@nestjs/common'
import { BeerService } from '../service/beer.service'
import { Beer } from '../entity/beer.entity'
import {
    ApiCreatedResponse,
    ApiOkResponse,
    ApiOperation,
    ApiTags,
    ApiUnprocessableEntityResponse,
    ApiNotFoundResponse
} from '@nestjs/swagger'
import { ExceptionMessages } from '../../middleware/exception/constants/constants'
import { ExceptionDto } from '../../middleware/exception/dto/exception.dto'
import { CreateBeerDto, CreateBeerUserDto } from '../dto/create-beer.dto'
import { CaslAbilityFactory } from '../../casl/factory/casl-ability.factory'
import { Auth } from '../../auth/decorator/auth.decorator'
import { UserRequestDto } from '../../auth/dto/UserRequest.dto'
import { UpdateBeerDto } from '../dto/update-beer.dto'
import { Action } from '../../casl/enum/action.enum'

@ApiTags('Beers')
@Controller('beers')
@UseInterceptors(ClassSerializerInterceptor)
export class BeerController {
    constructor(private beerService: BeerService, private caslAbilityFactory: CaslAbilityFactory) {}

    @Get()
    @ApiOperation({ summary: 'Retrieve all beers' })
    @ApiOkResponse({ type: Beer })
    async findAll(): Promise<Beer[]> {
        return this.beerService.findAll()
    }

    @Get(':name')
    @ApiOperation({ summary: 'Find a beer by name' })
    @ApiOkResponse({ type: Beer })
    @ApiNotFoundResponse({
        description: ExceptionMessages.NOT_FOUND_EXCEPTION,
        type: ExceptionDto
    })
    async findByName(@Param('name') name: string): Promise<Beer> {
        const beer = await this.beerService.findByName(name)
        if (!beer) {
            throw new NotFoundException(`Beer ${name} does not exist`)
        } else {
            return beer
        }
    }

    @Post()
    @Auth()
    @ApiOperation({ summary: 'Create a beer (add a beer to the store)' })
    @ApiCreatedResponse({ description: 'Beer successfully created', type: Beer })
    @ApiUnprocessableEntityResponse({
        description: ExceptionMessages.UNPROCESSABLE_EXCEPTION,
        type: ExceptionDto
    })
    async create(@Body() createBeerDto: CreateBeerDto, @Req() request: UserRequestDto) {
        const createBeerUserDto: CreateBeerUserDto = {
            ...createBeerDto,
            owner: request.user
        }
        const existingBeer = await this.beerService.findByName(createBeerUserDto.name)
        if (existingBeer) {
            throw new ConflictException(`Beer ${createBeerUserDto.name} already exists`)
        }
        try {
            return await this.beerService.create(createBeerUserDto)
        } catch (e) {
            throw new UnprocessableEntityException(e)
        }
    }

    @Patch(':id')
    @Auth()
    @ApiOperation({ summary: 'Update a beer' })
    @ApiOkResponse({ description: 'Beer successfully updated', type: Beer })
    @ApiNotFoundResponse({
        description: ExceptionMessages.NOT_FOUND_EXCEPTION,
        type: ExceptionDto
    })
    async update(@Req() request: UserRequestDto, @Param('id') id: number, @Body() updateBeerDto: UpdateBeerDto) {
        if (
            !this.caslAbilityFactory.createForUser(request.user).can(Action.Update, await this.beerService.findById(id))
        ) {
            throw new UnauthorizedException(`Cannot update a beer that was created by another user`)
        }
        await this.beerService.update(id, updateBeerDto)
    }
}
