import { Body, ClassSerializerInterceptor, Controller, Post, UseGuards, UseInterceptors } from '@nestjs/common'
import { AuthService } from '../service/auth.service'
import { LocalAuthGuard } from '../guard/local-auth.guard'
import { SignInDto } from '../dto/SignIn.dto'
import { ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger'
import { JwtTokenDto } from '../dto/JwtToken.dto'
import { ExceptionDto } from '../../middleware/exception/dto/exception.dto'
import { ExceptionMessages } from '../../middleware/exception/constants/constants'

@ApiTags('Auth')
@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {
    constructor(private authService: AuthService) {}

    @Post('signIn')
    @UseGuards(LocalAuthGuard)
    @ApiOperation({ summary: 'Log in as a customer or an administrator' })
    @ApiOkResponse({ type: JwtTokenDto })
    @ApiUnauthorizedResponse({
        description: ExceptionMessages.UNAUTHORIZED_EXCEPTION,
        type: ExceptionDto
    })
    async signIn(@Body() signIn: SignInDto) {
        return await this.authService.signIn(signIn)
    }
}
