import { ConfigModuleOptions } from '@nestjs/config'

export const load = (): ConfigModuleOptions => {
    const NODE_ENV = (process.env.NODE_ENV || 'dev') as 'test' | 'dev' | 'prod'

    return {
        isGlobal: true,
        envFilePath: NODE_ENV === 'dev' ? '.env' : '.env.test'
    }
}
