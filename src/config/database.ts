import { ConfigModule, ConfigService } from '@nestjs/config'
import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import { Beer } from '../beer/entity/beer.entity'
import { User } from '../user/entity/user.entity'

export const databaseOptions = (config: ConfigService): TypeOrmModuleOptions => {
    return {
        type: 'postgres',
        host: getOrThrow(config, 'POSTGRES_HOST'),
        port: parseInt(getOrThrow(config, 'POSTGRES_PORT') || '5432', 10),
        username: getOrThrow(config, 'POSTGRES_USERNAME'),
        password: getOrThrow(config, 'POSTGRES_PASSWORD'),
        database: getOrThrow(config, 'POSTGRES_DATABASE'),
        entities: [Beer, User],
        synchronize: true
    }
}

const getOrThrow = (config: ConfigService, name: string) => {
    const val = config.get<string>(name)
    if (typeof val === 'undefined') {
        throw new Error(`Missing mandatory environment variable ${name}`)
    }
    return val
}

export default () => ({
    imports: [ConfigModule],
    useFactory: (config: ConfigService) => databaseOptions(config),
    inject: [ConfigService]
})
