import { User } from '../entity/user.entity'
import { OmitType } from '@nestjs/swagger'

export class CreateUserDto extends OmitType(User, ['id'] as const) {}
