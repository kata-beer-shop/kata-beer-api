import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from '../entity/user.entity'
import { Repository } from 'typeorm'
import { CreateUserDto } from '../dto/create-user.dto'
import * as bcrypt from 'bcrypt'

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>
    ) {}

    async findAll() {
        return this.userRepository.find()
    }

    async findByEmail(email: string): Promise<User> {
        return this.userRepository.findOneBy({ email })
    }

    async create(user: CreateUserDto) {
        return this.userRepository.save(user)
    }

    async hashPassword(password: string) {
        return await bcrypt.hash(password, 10)
    }

    async verifyPassword(hash: string, plain: string) {
        return await bcrypt.compare(plain, hash)
    }
}
