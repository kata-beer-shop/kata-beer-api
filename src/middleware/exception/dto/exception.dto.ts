export class ExceptionDto {
    code: number
    message?: string
}
