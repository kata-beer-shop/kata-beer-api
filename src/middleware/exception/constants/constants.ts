export const ExceptionMessages = {
    UNAUTHORIZED_EXCEPTION: 'User is not allowed to perform operation',
    NOT_FOUND_EXCEPTION: 'Entity not found',
    UNPROCESSABLE_EXCEPTION: 'Entity is not well formatted'
}
