import * as request from 'supertest'
import { BeerModule } from '../../src/beer/module/beer.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { INestApplication } from '@nestjs/common'
import { Repository } from 'typeorm'
import { Test } from '@nestjs/testing'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { instanceToPlain } from 'class-transformer'
import { AuthModule } from '../../src/auth/module/auth.module'
import databaseOptions from '../../src/config/database'
import { load } from '../../src/config/config'
import { BeerService } from '../../src/beer/service/beer.service'
import { Beer } from '../../src/beer/entity/beer.entity'
import { getFakeBeer, getFakeBeerWithUser } from '../fake/beer.fake'
import { clearDatabase } from '../spec-helper'
import { AuthService } from '../../src/auth/service/auth.service'
import { UserService } from '../../src/user/service/user.service'
import { User } from '../../src/user/entity/user.entity'
import { JwtService } from '@nestjs/jwt'
import { getFakeUser } from '../fake/user.fake'

describe('BeerController (e2e)', () => {
    let beerService: BeerService
    let authService: AuthService
    let userService: UserService

    let beerRepository: Repository<Beer>
    let userRepository: Repository<User>

    let user: User
    let anotherUser: User

    let configService: ConfigService

    let app: INestApplication

    beforeAll(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [
                BeerModule,
                AuthModule,
                ConfigModule.forRoot(load()),
                TypeOrmModule.forRootAsync(databaseOptions())
            ]
        }).compile()

        app = moduleFixture.createNestApplication()
        await app.init()

        beerRepository = moduleFixture.get('BeerRepository')
        beerService = new BeerService(beerRepository)

        userRepository = moduleFixture.get('UserRepository')
        userService = new UserService(userRepository)

        configService = new ConfigService()

        authService = new AuthService(
            userService,
            new JwtService({
                secret: configService.get<string>('JWT_SECRET_KEY'),
                signOptions: { expiresIn: configService.get<string>('JWT_TOKEN_EXPIRATION') }
            })
        )
    })

    beforeEach(async () => {
        await clearDatabase(app)

        user = await userService.create(getFakeUser(1))
        anotherUser = await userService.create(getFakeUser(10))
    })

    it(`/GET beers`, async () => {
        await request(app.getHttpServer())
            .get('/beers')
            .expect(200)
            .expect(JSON.stringify(instanceToPlain(await beerService.findAll())))
    })

    it(`/GET beers/name`, async () => {
        const beer = await beerService.create(getFakeBeerWithUser(user))

        await request(app.getHttpServer())
            .get('/beers/' + beer.name)
            .expect(200)
            .expect(
                JSON.stringify(
                    instanceToPlain({
                        id: beer.id,
                        image: beer.image,
                        name: beer.name,
                        description: beer.description,
                        volume: beer.volume,
                        ingredients: beer.ingredients,
                        brewers_tips: beer.brewers_tips,
                        owner: {
                            id: beer.owner.id,
                            email: beer.owner.email,
                            role: beer.owner.role
                        }
                    })
                )
            )
    })

    it(`/POST beers`, async () => {
        const user = await userService.create(getFakeUser(1))
        const beer = getFakeBeer()
        const jwt = await authService.signIn(user)

        await request(app.getHttpServer())
            .post('/beers')
            .send(beer)
            .set('Authorization', 'Bearer ' + jwt.access_token)
            .expect(201)
            .expect(async () => {
                return JSON.stringify(instanceToPlain(await beerService.findAll()))
            })
    })

    it(`/PATCH beers/:id`, async () => {
        const user = await userService.create(getFakeUser(1))
        const beer = await beerService.create(getFakeBeerWithUser(user))
        const jwt = await authService.signIn(user)

        await request(app.getHttpServer())
            .patch('/beers/' + beer.id)
            .send({
                name: 'newbeername'
            })
            .set('Authorization', 'Bearer ' + jwt.access_token)
            .expect(200)
    })

    it(`/PATCH beers/:id from another user`, async () => {
        const user = await userService.create(getFakeUser(1))
        const beer = await beerService.create(getFakeBeerWithUser(anotherUser))
        const jwt = await authService.signIn(user)

        await request(app.getHttpServer())
            .patch('/beers/' + beer.id)
            .send({
                name: 'newbeername'
            })
            .set('Authorization', 'Bearer ' + jwt.access_token)
            .expect(401)
    })

    afterAll(async () => {
        await app.close()
    })
})
