import { faker } from '@faker-js/faker'
import { CreateBeerDto, CreateBeerUserDto } from '../../src/beer/dto/create-beer.dto'
import { User } from '../../src/user/entity/user.entity'

export const getFakeBeer = (): CreateBeerDto => {
    return {
        image: faker.internet.url(),
        name: faker.internet.userName(),
        description: faker.lorem.sentence(),
        volume: faker.number.int({ max: 500 }),
        ingredients: faker.lorem.sentence(),
        brewers_tips: faker.lorem.sentence()
    }
}

export const getFakeBeerWithUser = (user?: User): CreateBeerUserDto => {
    return {
        image: faker.internet.url(),
        name: faker.internet.userName(),
        description: faker.lorem.sentence(),
        volume: faker.number.int({ max: 500 }),
        ingredients: faker.lorem.sentence(),
        brewers_tips: faker.lorem.sentence(),
        owner: user
    }
}
