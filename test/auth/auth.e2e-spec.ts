import * as request from 'supertest'
import { UserModule } from '../../src/user/module/user.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { INestApplication } from '@nestjs/common'
import { User } from '../../src/user/entity/user.entity'
import { Repository } from 'typeorm'
import { UserService } from '../../src/user/service/user.service'
import { Test } from '@nestjs/testing'
import { ConfigModule } from '@nestjs/config'
import { clearDatabase } from '../spec-helper'
import { AuthModule } from '../../src/auth/module/auth.module'
import databaseOptions from '../../src/config/database'
import { load } from '../../src/config/config'

describe('AuthController (e2e)', () => {
    let userRepository: Repository<User>
    let app: INestApplication

    beforeAll(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [
                UserModule,
                AuthModule,
                ConfigModule.forRoot(load()),
                TypeOrmModule.forRootAsync(databaseOptions())
            ]
        }).compile()

        app = moduleFixture.createNestApplication()
        await app.init()

        userRepository = moduleFixture.get('UserRepository')
        new UserService(userRepository)
    })

    beforeEach(async () => {
        await clearDatabase(app)
    })

    it(`/POST signIn`, async () => {
        const payload = {
            email: 'testemail@gmail.com',
            password: 'hashpassword'
        }

        await request(app.getHttpServer()).post('/users').send(payload).expect(201)
        await request(app.getHttpServer()).post('/signIn').send(payload).expect(201)
    })

    afterAll(async () => {
        await app.close()
    })
})
