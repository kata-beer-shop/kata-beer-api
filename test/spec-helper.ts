import { DataSource } from 'typeorm'
import { Beer } from '../src/beer/entity/beer.entity'
import { User } from '../src/user/entity/user.entity'
import { INestApplication } from '@nestjs/common'

export const clearDatabase = async (app: INestApplication) => {
    const dataSource = app.get(DataSource)
    await dataSource.createQueryBuilder().delete().from(Beer).execute()
    await dataSource.createQueryBuilder().delete().from(User).execute()
}
